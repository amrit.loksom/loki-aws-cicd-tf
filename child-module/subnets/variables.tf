variable "vpc_id" {
  type        = string
  description = "dev vpc id"
}

variable "public_subnet_name" {
  type        = string
  description = "name for the vpc"
  default     = "dev_public_subnet"
}

variable "public_subnet_tag" {
  type        = string
  description = "tag for the public subnet"
  default     = "public"
}

variable "public_subnets" {
  type        = list(any)
  description = "list of public subnets"
  default     = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "azs" {
  type    = list(any)
  default = ["a", "b", "c"]
}