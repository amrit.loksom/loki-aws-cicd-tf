locals {
  region    = "eu-west-1"
  env       = "dev"
  nacl_name = "dev_vpc"
}

# 3 public subnet per AZ
resource "aws_subnet" "main-public" {
  vpc_id                  = var.vpc_id
  count                   = length(var.public_subnets)
  cidr_block              = var.public_subnets[count.index]
  availability_zone       = format("${local.region}%s", "${var.azs[count.index]}")
  map_public_ip_on_launch = "false"
  tags = {
    Name        = "${var.public_subnet_name}-${count.index + 1}"
    Environment = local.env
    Subnet      = var.public_subnet_tag
  }
}
