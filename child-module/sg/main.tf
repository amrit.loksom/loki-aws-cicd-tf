locals {
  region         = "eu-west-1"
  env            = "dev"
  inbound_ports  = [22, 80, 443]
  outbound_ports = [0]
}

# Security Groups
resource "aws_security_group" "dev-sg" {
  vpc_id      = var.vpc_id
  name        = var.sg_name
  description = "Security Group for dev"

  dynamic "ingress" {
    for_each = local.inbound_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  dynamic "egress" {
    for_each = local.outbound_ports
    content {
      from_port   = egress.value
      to_port     = egress.value
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  tags = {
    Environment = local.env
    Name        = var.sg_name
  }
}
