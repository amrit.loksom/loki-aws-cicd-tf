variable "sg_name" {
  type        = string
  description = "security group name"
  default     = "dev_sg"
}

variable "vpc_id" {
  type        = string
  description = "dev vpc id"
}

variable "public_subnet_name" {
  type        = string
  description = "name for the vpc"
  default     = "dev_public_subnet"
}
