variable "vpc_cidr" {
  type        = string
  description = "The IP range to use for the VPC"
  # default     = "10.0.0.0/16"
}

variable "vpc_name" {
  type        = string
  description = "name for the vpc"
  # default     = "dev_vpc"
}
