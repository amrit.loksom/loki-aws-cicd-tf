variable "vpc_id" {
  type        = string
  description = "dev vpc id"
}

variable "gw_name" {
  type        = string
  description = "name for the vpc"
  default     = "dev_gw"
}

variable "public_subnet_name" {
  type        = string
  description = "name for the vpc"
  default     = "dev_public_subnet"
}

variable "public_subnet_tag" {
  type        = string
  description = "tag for the public subnet"
  default     = "public"
}
