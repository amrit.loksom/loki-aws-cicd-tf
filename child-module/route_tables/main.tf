locals {
  region = "eu-west-1"
  env    = "dev"

}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [var.vpc_id]
  }

  tags = {
    Environment = local.env
    Subnet      = var.public_subnet_tag
  }
}


# Internet Gateway
resource "aws_internet_gateway" "main-gw" {
  vpc_id = var.vpc_id
  tags = {
    Name        = "${var.gw_name}"
    Environment = local.env
  }
}

# Route tables
resource "aws_route_table" "main-public" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main-gw.id
  }

  tags = {
    Name        = "${var.public_subnet_name}-GW-route-table"
    Environment = local.env
    Subnet      = var.public_subnet_tag
  }
}

# Route association GW and Public subnet

resource "aws_route_table_association" "main-public-table-association-1" {
  for_each       = toset(data.aws_subnets.public.ids)
  subnet_id      = each.value
  route_table_id = aws_route_table.main-public.id
}
