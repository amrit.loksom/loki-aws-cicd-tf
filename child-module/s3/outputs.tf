output "state_bucket_arn" {
  value = aws_s3_bucket.dev-bucket.arn
}

output "dynamo_lock_table" {
  value = aws_dynamodb_table.terraform-locks.id
}