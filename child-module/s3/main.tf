locals {
  region = "eu-west-1"
  env    = "dev"

}

resource "aws_s3_bucket" "dev-bucket" {
  bucket = "${var.bucket_prefix}-${local.env}-terraform-backend"

  lifecycle {
    prevent_destroy = false
  }

  tags = {
    Name        = "${var.bucket_prefix}-${local.env}-terraform-backend"
    Environment = local.env
  }
}

resource "aws_s3_bucket_versioning" "backend" {
  bucket = aws_s3_bucket.dev-bucket.id

  versioning_configuration {
    status = "Disabled"
  }
}

resource "aws_s3_bucket_server_side_encryption_configuration" "backend" {
  bucket = aws_s3_bucket.dev-bucket.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = var.bucket_sse_algorithm
    }
  }
}

resource "aws_dynamodb_table" "terraform-locks" {
  name         = "${var.bucket_prefix}-${local.env}-state-locking"
  billing_mode = "PAY_PER_REQUEST"
  hash_key     = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }
}

resource "aws_s3_bucket_acl" "dev-bucket-acl" {
  bucket = aws_s3_bucket.dev-bucket.id
  acl    = "private"
}
