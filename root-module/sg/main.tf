locals {
  region         = "eu-west-1"
  env            = "dev"
  inbound_ports  = [22, 80, 443]
  outbound_ports = [0]
}

module "dev_sg" {
  source   = "../../child-module/sg"
  vpc_id   = var.vpc_id
  sg_name  = var.sg_name
}