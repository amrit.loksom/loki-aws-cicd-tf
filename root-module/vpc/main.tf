locals {
  region = "eu-west-1"
  env    = "dev"

}

module "tf_vpc" {
  source   = "../../child-module/vpc"
  vpc_cidr = var.vpc_cidr
  vpc_name = var.vpc_name
}