locals {
  region = "eu-west-1"
  env    = "dev"

}

module "tf_rt_table" {
  source = "../../child-module/route_tables"
  vpc_id = var.vpc_id
}