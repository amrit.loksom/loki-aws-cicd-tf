locals {
  region    = "eu-west-1"
  env       = "dev"
  nacl_name = "dev_vpc"
}

module "tf_subnet" {
  source = "../../child-module/subnets"
  vpc_id = var.vpc_id
}