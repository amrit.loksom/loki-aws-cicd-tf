# S3

Terraform module for AWS S3 bucket.

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 0.13.1 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.63 |

## Providers

No providers.

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_s3"></a> [s3](#module\_S3) | ../../child-module/s3 | n/a |

## Resources

No resources.

## Inputs

| Name | Description |
|------|-------------|
| <a name="input_bucket_prefix"></a> [bucket\_prefix](#input\_bucket\_prefix) | s3 bucket prefix used for unique bucket name |
| <a name="input\_bucket\_sse\_algorithm"></a> [bucket_sse_algorithm](#input\_bucket\_sse\_algorithm) | Algorithm used for S3 server side encryption |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output\_s3\_bucket\_arn"></a> [s3_bucket_arn](#output\_s3\_bucket\_arn) | ARN for s3 bucket |
| <a name="output\_dynamo\_lock\_table"></a> [dynamo_lock_table](#output\_dynamo\_lock\_table) | DynamoDB table name for state locking |