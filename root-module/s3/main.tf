locals {
  region = "eu-west-1"
  env    = "dev"

}

module "tf_backend" {
  source = "../../child-module/s3"

  bucket_prefix        = var.bucket_prefix
  bucket_sse_algorithm = var.bucket_sse_algorithm

}