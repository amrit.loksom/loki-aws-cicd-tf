# loki-tf-cicd-example pipeline

This is an example pipeline to deploy terraform modules.

This repo using terraform modules, docker images, ci/cd pipeline deploys AWS VPC.

To store tf remote backend on S3 we need to create S3 first.

# Main branch
The underlying AWS infrastructure has to be set up through the main branch. <br>
The following AWS resources will be set up through the main branch CI pipeline:
1. S3 (manually create locally and migrate the tfstate to s3)
2. VPC (need to set vpc_id variable in gitlab once created, terraform output should be presented once job is finished)
3. Internet Gateway (for internet access)
4. Subnets (3 public subnets)
5. Route tables (associated with subnets created)
6. Security Group (security group with ingress rule for TCP 22,80,443 and egress for all ports)

# App Branch
The following AWS resources will be set up (with the resources created in main branch) through the app branch CI pipeline:
1. ALB (listens to port 80 and forwards to LB target group)
2. ASG (attached and registered to target group created above)

# Setting up local gitlab runner
[Install Gitlab runner link](https://docs.gitlab.com/runner/install/) <br>
Choose a suitable name for runner <br>
Assign tags: localrunner, localshell 

# Creating S3 bucket

1. Clone the repo locally
2. cd to root-module/s3
3. initialize terraform by running following command
```
terraform init
```
4. validate the tf code
```
terraform validate
```
5. run a terraform plan
```
terraform plan
```
6. run terraform apply if terraform plan is successful. type yes when prompted for approval
```
terraform apply
```
Terraform output will be available once successfully run.

Note the outputs as this will be useful in the next step to store your Environment variables.

You should now have s3 bucket created in your AWS account. To move your s3 tfstate file to s3 bucket, uncomment and complete the terraform backend block in providers.tf file. Then save the file and run the command: terraform init

# Set up CI/CD variables 
##### Note: protected variables are only visible to protected branches

On your project, go to Settings > CI/CD > Variables. Then add the following keys and values.

AWS_ACCESS_KEY_ID: <get from ~/.aws/credentials> <br>
AWS_SECRET_ACCESS_KEY: <get from ~/.aws/credentials> <br>
AWS_DEFAULT_REGION: <set aws region> <br>
TF_BUCKET: <get info from terraform output> <br>
TF_VPC_STATE_KEY: dev/dev-vpc.tfstate <br>
DYNAMODB_TABLE: <copy info from terraform output> 

You can also get terraform output by changing directory to root-module/s3 and running the following command.
```
terraform output
```

Re-run your pipeline.

Provided that the parameters and environment variables have been configured correctly the pipeline should create a VPC in your specified region.

[s3 guide for this project](root-module/s3/README.md) <br>
[vpc guide for this project](root-module/vpc/README.md)
