FROM hashicorp/terraform:latest
WORKDIR /data
ADD . /data
ADD ./backend-config/config.loki-tf-s3.backend /data/backend-config/config.loki-tf-s3.backend
RUN apk add --no-cache jq \
    python3 \
    py3-pip && \
    pip3 install --upgrade pip && \
    pip3 install --no-cache-dir awscli && \
    rm -rf /var/cache/apk/*
RUN aws --version
ENTRYPOINT ["/bin/sh"]